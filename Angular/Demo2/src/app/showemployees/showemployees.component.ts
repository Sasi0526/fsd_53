import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrl: './showemployees.component.css'
})
export class ShowemployeesComponent implements OnInit {

  employees: any;

  constructor() {
    this.employees = [
      { id:101, name:'Sasi', salary: 4545.00, gender: 'male',doj:'02-05-2023', emailId: 'sasi@gmail.com', password:123,deptId: 10 },
      { id:102, name:'Vamsi', salary: 5454.00, gender: 'male',doj:'03-25-2022', emailId: 'vamsi@gmail.com',password:123,deptId: 20 },
      { id:103, name:'Subash', salary: 5654.00, gender: 'male',doj:'05-15-2021', emailId: 'subash@gmail.com',password:123,deptId: 30 },
      { id:104, name:'Robin', salary: 6464.00, gender: 'female',doj:'07-17-2000', emailId: 'robin@gmail.com',password:123,deptId: 40 },
      { id:105, name:'Radha', salary: 7548.00, gender: 'female',doj:'09-22-1999', emailId: 'radha@gmail.com',password:123,deptId: 50 },
    ]
  }
  ngOnInit() {
    
  }
}
