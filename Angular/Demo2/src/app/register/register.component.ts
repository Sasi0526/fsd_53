import { Component } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {

  employee: any;

  constructor() {
    this.employee = {
      empId: '', 
      empName: '',
      salary: '',
      gender: '',
      doj: '',
      country: '',
      emailId: '',
      password: '',
      deptId: ''
    }
  }

  ngOnInit(): void {
  }

  submit() {
    console.log(this.employee);
  }

  }



