import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { Test2Component } from './test2/test2.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

import { FormsModule } from '@angular/forms';
import { ShowemployeesComponent } from './showemployees/showemployees.component';
import { ExpPipe } from './exp.pipe';
import { GenderPipe } from './gender.pipe';
import { EmployeebyidComponent } from './employeebyid/employeebyid.component';
import { EmploginComponent } from './emplogin/emplogin.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    Test2Component,
    LoginComponent,
    RegisterComponent,
    ShowemployeesComponent,
    ExpPipe,
    GenderPipe,
    EmployeebyidComponent,
    EmploginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule        //Add forms module for 2-way data binding.
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
