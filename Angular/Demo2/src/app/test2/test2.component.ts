import { Component,OnInit } from '@angular/core';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrl: './test2.component.css'
})
export class Test2Component implements OnInit {
  person: { id: number; name: string; age: number; address: { streetNo: number; city: string; state: string; }; hobbies: string[]; };

  constructor(){
    console.log('Constructor Invoked...');

    this.person = {
      id: 101,
      name: 'Sasi',
      age: 22,
      address: {streetNo: 101, city: 'Hyderabad', state:'Telangana'},
      hobbies: ['Running', 'Walking', 'Swimming', 'Music', 'Cricket']
    }

  }

  ngOnInit(): void {
    //alert('ngOnInit Invoked...');
    console.log('ngOnInit Invoked...');
  }

  showDetails(){
    console.log(this.person)
  }

}
