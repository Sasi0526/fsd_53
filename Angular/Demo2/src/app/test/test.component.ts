import { Component } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent {

  id:number;
  name:String;
  age:number;

  address:any;
  hobbies:any;

  constructor(){
    this.id=101;
    this.name="sasi";
    this.age=20;

    this.address={streetNo:101,city:"hyd",state:'Telengana'};
    this.hobbies=['playing cricket','reading books']

  }

}
