import { Component } from '@angular/core';

@Component({
  selector: 'app-test',
  standalone: true,
  imports: [],
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent {

  id: 101;
  name: "Sasi";
  age: 22;
  address: any;
  hobbies: any;

  constructor(){
    this.id=101;
    this.name="Sasi";
    this.age=22;

    this.address={StreetNo:101,city:'hyd',state:'telengana'};
    this.hobbies=['Reading','playing chess','travelling'];

  }

}
